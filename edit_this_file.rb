class HelloWorld
  def initialize(name)
    @name = name.capitalize
  end
  # working here
  def say_hi
    puts "Hello #{@name}!"
  end
# 2nd August 2018 9:12PM IST

  # TODO: Remove this comment and the method below.
  def say_bye
    puts "Good-bye #{@name}!"
  end
end
